#pragma once

#include <functional>
#include <algorithm>

template <class S> using Provider = std::function< S () >;

template <class E> using Predicate = std::function< bool ( E ) >;

template <class T, class S> using Mapper = std::function< S ( T ) >;

template <class R> using Consumer = std::function< void ( R ) >;

template <class T> using BinaryOperation = std::function< T ( T, T ) >;

using Action = std::function< void () >;