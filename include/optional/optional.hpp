#pragma once

#include <stdexcept>

#include "../lambda/lambda.hpp"
#include "exception/nullptr-exception.hpp"

template <class T> class Optional {

	public:

		static Optional<T> of( T const t ){ return Optional<T>( t ); }

		static Optional<T> ofNullable( T const t ){ return Optional<T>( t ); }

		T const get() const { return t; };
	
		T const orElse( T const other ) const { return other; };		

		T const orElseGet( Provider<T const> const provider ){
			if( provider == nullptr ) throw std::invalid_argument("[OPTIONAL::orElseGet] - Provider cannot be NULL!!");
			return provider();
		}

		template <class E> Optional<E> map( Mapper<T const, E const> const mapper ){
			if( mapper == nullptr ) throw std::invalid_argument("[OPTIONAL::map] - Mapper cannot be NULL!!");
			E const map_value = mapper( t );
			return Optional<E>::of( map_value );
		}

		void execute( Consumer<T const> const consumer ){
			if( consumer == nullptr ) throw std::invalid_argument("[OPTIONAL::execute] - Consumer cannot be NULL!!");
			consumer( t );
		}
		
	private:

		T const t;

		Optional( T const t ) : t( t ){}

};

#include "optional-pointer.hpp"
