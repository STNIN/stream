#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void allMatchPrimitivePredicateNull(){
    try{
        Stream<int>::of( { 10 } ).allMatch( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[allMatchPrimitivePredicateNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void allMatchPrimitivePredicateTrue(){
    assert( ( Stream<int>::of( { 10, 10, 10, 10, 10 } ).allMatch( []( int const value ){ return value == 10; } ) ) );
    std::cout << "[TEST]::[allMatchPrimitivePredicateTrue]::SUCCESS" << std::endl;
}

void allMatchPrimitivePredicateFalse(){
    assert( ( !Stream<int>::of( { 10, 10, 10, 10, 20 } ).allMatch( []( int const value ){ return value == 10; } ) ) );
    std::cout << "[TEST]::[allMatchPrimitivePredicateFalse]::SUCCESS" << std::endl;
}

void allMatchAbstractPredicateNull(){
    class Test{ public: int const x = 10; };
    try{
        Stream<Test>::of( { Test() } ).allMatch( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[allMatchAbstractPredicateNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void allMatchAbstractPredicateTrue(){
    class Test{ public: int const x = 10; };
    assert( ( Stream<Test>::of( { Test(), Test() } ).allMatch( []( Test const value ){ return value.x == 10; } ) ) );
    std::cout << "[TEST]::[allMatchAbstractPredicateTrue]::SUCCESS" << std::endl;
}

void allMatchAbstractPredicateFalse(){
    class Test{ public: int x = 10; };
    Test test = Test();
    test.x = 20;
    assert( ( !Stream<Test>::of( { Test(), test } ).allMatch( []( Test const value ){ return value.x == 10; } ) ) );
    std::cout << "[TEST]::[allMatchAbstractPredicateFalse]::SUCCESS" << std::endl;
}

void allMatchPrimitiveEmpty(){
    assert( ( !Stream<int>::empty().allMatch( []( int const value ){ return value == 10; } ) ) );
    std::cout << "[TEST]::[allMatchPrimitiveEmpty]::SUCCESS" << std::endl;
}

void allMatchAbstractEmpty(){
    class Test{ public: int x = 10; };
    assert( ( !Stream<Test>::empty().allMatch( []( Test const value ){ return value.x == 10; } ) ) );
    std::cout << "[TEST]::[allMatchAbstractEmpty]::SUCCESS" << std::endl;
}