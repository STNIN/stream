#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void toArray(){
    auto array = Stream<int>::of( {10, 20, 30} ).toArray();
    assert( array.array[0] == 10 );
    assert( array.array[1] == 20 );
    assert( array.array[2] == 30 );
    assert( array.length == 3 );
    std::cout << "[TEST]::[toArray]::SUCCESS" << std::endl;
}