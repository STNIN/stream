#include "of-test.hpp"
#include "empty-test.hpp"
#include "isEmpty-test.hpp"
#include "allMatch-test.hpp"
#include "anyMatch-test.hpp"
#include "forEach-test.hpp"
#include "filter-test.hpp"
#include "toArray-test.hpp"
#include "map-test.hpp"
#include "reduce-test.hpp"
#include "findFirst-test.hpp"
#include "limit-test.hpp"

int main(){
    
    ofPrimitiveArray();
    ofAbstractArray();

    emptyPrimitive();
    emptyAbstract();

    isEmptyPrimitiveTrue();
    isEmptyPrimitiveFalse();
    isEmptyAbstractTrue();
    isEmptyAbstractFalse();

    allMatchPrimitivePredicateNull();
    allMatchPrimitivePredicateTrue();
    allMatchPrimitivePredicateFalse();
    allMatchAbstractPredicateNull();
    allMatchAbstractPredicateTrue();
    allMatchAbstractPredicateFalse();
    allMatchPrimitiveEmpty();
    allMatchAbstractEmpty();

    anyMatchPrimitivePredicateNull();
    anyMatchPrimitivePredicateTrue();
    anyMatchPrimitivePredicateFalse();
    anyMatchAbstractPredicateNull();
    anyMatchAbstractPredicateTrue();
    anyMatchAbstractPredicateFalse();
    anyMatchPrimitiveEmpty();
    anyMatchAbstractEmpty();

    forEachPrimitiveConsumerNull();
    forEachPrimitiveConsumer();
    forEachAbstractConsumerNull();
    forEachAbstractConsumer();
    forEachPrimitiveEmpty();
    forEachAbstractEmpty();

    filterPrimitivePredicateNull();
    filterPrimitivePredicate();
    filterAbstractPredicateNull();
    filterAbstractPredicate();
    filterPrimitiveEmpty();
    filterAbstractPredicateEmpty();

    toArray();

    mapPrimitiveMapperNull();
    mapPrimitiveMapperPrimitive();
    mapPrimitiveMapperAbstract();
    mapAbstractMapperNull();
    mapAbstractMapperPrimitive();
    mapAbstractMapperAbstract();
    mapPrimitiveEmpty();
    mapAbstractEmpty();

    reducePrimitiveBinaryOperationsNull();
    reducePrimitiveBinaryOperationsEmpty();
    reducePrimitiveBinaryOperations();
    reduceAbstractBinaryOperationsNull();
    reduceAbstractBinaryOperationsEmpty();
    reduceAbstractBinaryOperations();

    findFirstPrimitiveEmpty();
    findFirstPrimitive();
    findFirstAbstractEmpty();
    findFirstAbstract();

    limitPrimitiveEmpty();
    limitPrimitive();
    limitiAbstractEmpty();
    limitAbstract();

    return 0;

}